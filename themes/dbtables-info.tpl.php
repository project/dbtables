<?php

/**
 * @file
 * Theme for DBTables Info page.
 */
?>
<div class="dbtable-info">
  <?php echo $dbtable_tabs; ?>
  <?php echo $dbtable_desc; ?>
  <?php echo $dbtable_info; ?>
</div>
