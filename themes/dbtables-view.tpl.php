<?php

/**
 * @file
 * Theme for DBTables View page.
 */
?>
<div class="dbtable-view">
  <?php echo $dbtable_tabs; ?>
  <?php echo $table; ?>
  <?php echo $pager; ?>
</div>
