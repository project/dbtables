<?php

/**
 * @file
 * Theme for DBTables tabs.
 */
?>
<div class="dbtables-tabs-container">
  <ul class="tabs primary">
    <?php foreach ($menu as $el) : ?>
      <?php echo $el ?>
    <?php endforeach; ?>
  </ul>
</div>
