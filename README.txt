-- SUMMARY --
This module allows the user to have a look to Drupal database tables of contents.
Quite often we need to carry quick check on database,
for example to see if the data have already been stored in tables;
this module will be handy to have to that purpose.

-- FEATURES --
- Allows to look at the schema of each table of the Drupal db.
- Allows to look at the content of each table of the Drupal db.

-- REQUIREMENTS --
This module doesn't require any other module to be enabled.

-- INSTALLATION --
* To install the module copy the 'dbtables' folder to
   your sites/all/modules/contrib directory.
* Go to admin/build/modules. Enable the module.
   Read more about installing modules at http://drupal.org/node/70151
* Go to admin/content/dbtables to start.

-- CONFIGURATION --
* Configure user permissions in Administration -> People -> Permissions.
* This module uses the permission 'administer dbtables'.

-- CONTACT --
Current maintainers:
* Gabriele Manna (geberele) - https://drupal.org/user/1183748
